package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	_ "github.com/alexbrainman/odbc"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

var (
	otdel     []string
	fam, name string
	ot        string
	id, j     int
	id_otdel  string
	date_post time.Time
	options   = map[int]Option{}
	selected  string

	name_otdel string
)

type Option struct {
	Value, Id, Text string
	Selected        string
}

func index(w http.ResponseWriter, r *http.Request) {
	var i int
	w.Header().Set("Content-type", "text/html")
	otdel := r.PostFormValue("otdelenie")
	t, _ := template.ParseFiles("main.html")
	if r.FormValue("otdelenie") != "" {
		selected = r.FormValue("otdelenie")
	}
	db, err := sql.Open("odbc", "DSN=DBS0")
	if err != nil {
		fmt.Println("Error in connect DB")
		log.Fatal(err)
	}
	rows_name_otdel, err := db.Query("select t.DepartmentID, t.DepartmentNAME from dbo.oms_Department t where t.rf_LPUID = 1078")
	if err != nil {
		log.Fatal(err)
	}
	defer rows_name_otdel.Close()
	j = 0
	for rows_name_otdel.Next() {
		if err := rows_name_otdel.Scan(&id_otdel, &name_otdel); err != nil {
			log.Fatal(err)
		}
		//Перекодируем данные из MS SQL DB в UTF8
		sr_name_otdel := strings.NewReader(name_otdel)
		tr := transform.NewReader(sr_name_otdel, charmap.Windows1251.NewDecoder())
		buf, err := ioutil.ReadAll(tr)
		if err != err {
			// обработка ошибки
		}
		name_otdel_utf := string(buf) // строка в UTF-8

		options[j] = Option{id_otdel, "Id1", name_otdel_utf, selected}
		j++
	}
	t.Execute(w, options)

	if otdel == "" {
		otdel = "157"
	}
	rows, err := db.Query("select hDED.FAMILY, hDED.Name, hDED.OT, hDED.DateRecipient, br.rf_departmentid from dbo.stt_medicalhistory hDED inner join  dbo.v_curentmigrationpatient AS m on hDED.medicalhistoryid = m.rf_medicalhistoryid  INNER JOIN dbo.stt_stationarbranch AS br ON br.stationarbranchid = m.rf_stationarbranchid and br.rf_departmentid=?", &otdel)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	//defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&fam, &name, &ot, &date_post, &id); err != nil {
			log.Fatal(err)
		}
		//Перекодируем данные из MS SQL DB в UTF8
		sr_fam := strings.NewReader(fam)
		tr := transform.NewReader(sr_fam, charmap.Windows1251.NewDecoder())
		buf, err := ioutil.ReadAll(tr)
		if err != err {
			// обработка ошибки
		}
		fam_utf := string(buf) // строка в UTF-8

		sr_name := strings.NewReader(name)
		tr_name := transform.NewReader(sr_name, charmap.Windows1251.NewDecoder())
		buf1, err := ioutil.ReadAll(tr_name)
		if err != err {
			// обработка ошибки
		}
		name_utf := string(buf1) // строка в UTF-8

		sr_ot := strings.NewReader(ot)
		tr_ot := transform.NewReader(sr_ot, charmap.Windows1251.NewDecoder())
		buf2, err := ioutil.ReadAll(tr_ot)
		if err != err {
			// обработка ошибки
		}
		ot_utf := string(buf2) // строка в UTF-8
		i = i + 1

		date_post_format := date_post.Format(time.RFC822)

		fmt.Fprint(w, "<tr><td>", i, "</td>", "<td>", fam_utf, "</td>", "<td>", name_utf, "</td><td>", ot_utf, "</td><td>", date_post_format, "</td><td></td><td></td></tr>\n")
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	defer db.Close()
	fmt.Fprintf(w, "</tbody></table>")
	t3, _ := template.ParseFiles("futer.html")
	t3.Execute(w, nil)
}

func main() {
	http.HandleFunc("/", index)
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("./css/"))))
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("./js/"))))
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("./images/"))))
	http.ListenAndServe(":8080", nil)
}
